#!/usr/bin/env ruby

# Set a function to return a user's highest role
def highest_role(user)
  Gitlab::Access.human_access_with_none(user.highest_role)
end

# Set a function to make a string showing user info
def user_info(user)
  "#{user.id}, #{user.username}, #{highest_role(user)}, signed in #{user.sign_in_count} time(s), #{user.last_sign_in_at}"
end

# Setup users hash
users = {
  owner: [],
  maintainer: [],
  reporter: [],
  developer: [],
  guest: [],
  none: [],
  blocked: [],
  deactivated: []
}
# Set an array to have a list of users to ignore
ignore = %w[ghost support-bot alert-bot]
# Set an integer to zero to count active users
active = 0
# Set an integer to zero to count trueup users
trueup = 0

# Find all users and iterate over them
User.all.each do |user|
  # Skip iteration if the user is one of the non-counted system users
  next if ignore.include? user.username

  # Push to the Blocked array if the user is blocked
  users[:blocked].push(user) if user.blocked?
  # Skip iteration if the user is blocked
  next if user.blocked?

  # Push to the Blocked array if the user is deactivated
  users[:deactivated].push(user) if user.deactivated?
  # Skip iteration if the user is blocked
  next if user.deactivated?


  # Start a case statement using the return from the highest_role function
  case highest_role(user)
  when 'Owner' # When the user's highest role is Owner
    users[:owner].push(user)
  when 'Maintainer' # When the user's highest role is Maintainer
    users[:maintainer].push(user)
  when 'Reporter' # When the user's highest role is Reporter
    users[:reporter].push(user)
  when 'Developer' # When the user's highest role is Developer
    # Push to Low sign ins array unless the user signed in 3 or more times
  when 'Guest' # When the user's highest role is Guest
    users[:guest].push(user)
  when 'None' # When the user doesn't have any roles
    # Push the None array in the users hash
    users[:none].push(user)
  end
end

# Setup a to_count variable and set it to zero
to_count = 0
# Setup a not_to_count variable and set it to zero
not_to_count = 0

# Iterate over our users hash
users.each do |role, us|
  # Output the role name and the count of users in it
  puts "#{role.to_s.capitalize}: #{us.count}"
  # If the environment variable VERBOSE is set to a true value
  if ENV['VERBOSE']
    # Iterate over all the users in the array, outputing the user_info string
    us.each { |u| puts "\t#{user_info(u)}" }
  end
  # Add the role's array count to to_count unless they are in the None, Blocked, Deactivated
  to_count += us.count unless ['none', 'blocked', 'deactivated',].include? role.to_s
  # Add the role's array count to not_to_count if they are in the None, Blocked, or Deactivated
  not_to_count += us.count if ['none', 'blocked', 'deactivated'].include? role.to_s
end

# Output an empty line
puts
# Output the to_count total count
puts "To Count:     #{to_count}"
# Output the not_to_count total count
puts "Not to Count: #{not_to_count}"

